<?php
session_start();
if (!isset($_SESSION['user'])) {
  // jika user belum login
  header('Location: ../login');
  exit();
}

include('../../config/koneksi.php');
include('../_partials/uuid.php');

// ambil data dari form
$uuid = htmlspecialchars($_POST['uuid']);
$NIK = htmlspecialchars($_POST['NIK']);
$No_KK = htmlspecialchars($_POST['No_KK']);
$No_akta = htmlspecialchars($_POST['No_akta']);
$Nama = htmlspecialchars($_POST['Nama']);
$Jenis_kelamin = htmlspecialchars($_POST['Jenis_kelamin']);
$Tempat_lahir = htmlspecialchars($_POST['Tempat_lahir']);
$Tanggal_lahir = htmlspecialchars($_POST['Tanggal_lahir']);

$Status_perkawinan = htmlspecialchars($_POST['Status_perkawinan']);
$Agama = htmlspecialchars($_POST['Agama']);
$Pendidikan = htmlspecialchars($_POST['Pendidikan']);
$Pekerjaan = htmlspecialchars($_POST['Pekerjaan']);
$Kewarganegaraan = htmlspecialchars($_POST['Kewarganegaraan']);
$Alamat = htmlspecialchars($_POST['Alamat']);
$Anak_ke = htmlspecialchars($_POST['Anak_ke']);
$Status_keluarga = htmlspecialchars($_POST['Status_keluarga']);

$query = "UPDATE penduduk SET NIK='$NIK', No_KK='$No_KK', No_akta='$No_akta', Nama='$Nama', Jenis_kelamin='$Jenis_kelamin', Tempat_lahir='$Tempat_lahir', Tanggal_lahir='$Tanggal_lahir', Status_perkawinan='$Status_perkawinan', Agama='$Agama', Pendidikan='$Pendidikan', Pekerjaan='$Pekerjaan', Kewarganegaraan='$Kewarganegaraan', Alamat='$Alamat', Anak_ke='$Anak_ke', Status_keluarga='$Status_keluarga' WHERE uuid='$uuid';";

// $query = "UPDATE penduduk SET NIK = '$NIK', No_KK = '$No_KK', No_akta = '$No_akta', Nama = '$Nama', Jenis_kelamin = '$Jenis_kelamin', Tempat_lahir = '$Tempat_lahir', Tanggal_lahir = '$Tanggal_lahir', Status_perkawinan = '$Status_perkawinan', Agama = '$Agama', Pendidikan = '$Pendidikan', Pekerjaan = '$Pekerjaan', Kewarganegaraan = '$Kewarganegaraan', Alamat = '$Alamat', Anak_ke = '$Anak_ke', Status_keluarga = '$Status_keluarga' Where uuid = '$uuid';";
$hasil = mysqli_query($db, $query);
echo json_encode ($hasil);

// cek keberhasilan pendambahan data
if ($hasil) {
  echo "<script>window.alert('Update Data Penduduk berhasil'); window.location.href='../KTP/index.php'</script>";
} else {
//  echo "<script>window.alert('Update Data Penduduk gagal!' . mysqli_error($db)); window.location.href='../KTP/index.php'</script>";
    echo "ERROR: Could not able to execute $query. " . mysqli_error($db);
}
