<?php include('../_partials/top.php') ?>

<h1 class="page-header">Data KTP</h1>
<?php include('_partials/menu.php') ?>

<?php include('data-show.php') ?>

<form action="update.php" method="post">
<table class="table table-striped table-middle">
	<input type="hidden" class="form-control" name="uuid" value="<?php echo $data_KTP[0]['uuid']; ?>">
  <tr>
    <th width="20%">NIK</th>
    <td width="1%">:</td>  
    <td><input type="number" class="form-control" name="NIK" value="<?php echo $data_KTP[0]['NIK']; ?>"></td>
  </tr>
  <tr>
    <th width="20%">No KK</th>
    <td width="1%">:</td>  
    <td><input type="number" class="form-control" name="No_KK" value="<?php echo $data_KTP[0]['No_KK'] ?>"></td>
  </tr>
  <tr>
    <th width="20%">No akta</th>
    <td width="1%">:</td>  
    <td><input type="number" class="form-control" name="No_akta" value="<?php echo $data_KTP[0]['No_akta']; ?>"></td>
  </tr>
  <tr>
    <th>Nama</th>
    <td>:</td>
    <td><input type="text" class="form-control" name="Nama" value="<?php echo $data_KTP[0]['Nama']; ?>" required></td>
  </tr>
   <tr>
    <th>Jenis Kelamin</th>
    <td>:</td>
    <td>
      <select class="form-control selectpicker" name="Jenis_kelamin" required>
	  value="<?php echo $data_KTP[0]['Jenis_kelamin']; ?>"
	  <?php
		if($data_KTP[0]['Jenis_kelamin'] == 'L'){
	  ?>
		<option value="L">Laki-laki</option>
        <option value="P">Perempuan</option>
	  <?php
		}else{
	  ?>
        <option value="P">Perempuan</option>
		<option value="L">Laki-laki</option>
	  <?php
		}
	  ?>
      </select>
    </td>
  </tr>
  <tr>
    <th>Tempat Lahir</th>
    <td>:</td>
    <td><input type="text" class="form-control" name="Tempat_lahir" value="<?php echo $data_KTP[0]['Tempat_lahir']; ?>" required></td>
  </tr>
  <tr>
    <th>Tanggal Lahir</th>
    <td>:</td>
    <td><input type="text" class="form-control datepicker" name="Tanggal_lahir" value="<?php echo is_object($data_KTP[0]['Tanggal_lahir']) ? $data_KTP[0]['Tanggal_lahir']->format("Y-m-d") : $data_KTP[0]['Tanggal_lahir']?>" required></td>
  </tr>
  <tr>
    <th>Agama</th>
    <td>:</td>
    <td>
		<select class="form-control selectlive" name="Agama"  >
	        <option value="<?php echo $data_KTP[0]['Agama'] ?>" selected>Pilih</option> 
	        <option value="Islam">Islam</option>
	        <option value="Kristen">Kristen</option>
	        <option value="Katholik">Katholik</option>
	        <option value="Hindu">Hindu</option>
	        <option value="Budha">Budha</option>
	    </select>
	</td>
  </tr>
  <tr>
    <th>Status perkawinan</th>
    <td>:</td>
    <td>
        <select class="form-control selectpicker" name="Status_perkawinan"  required>
	        <option value="<?php echo $data_KTP[0]['Status_perkawinan']; ?>" selected>Pilih</option>
            <option value="Belum Menikah">Belum Menikah</option>
            <option value="Sudah Menikah">Sudah Menikah</option>
        </select>
    </td>
  </tr>
  <tr>
    <th>Pendidikan Terakhir</th>
    <td>:</td>
    <td><input type="text" class="form-control" name="Pendidikan" value="<?php echo $data_KTP[0]['Pendidikan']; ?>"  ></td>
  </tr>
  <tr>
    <th>Pekerjaan</th>
    <td>:</td>
    <td><input type="text" class="form-control" name="Pekerjaan" value="<?php echo $data_KTP[0]['Pekerjaan']; ?>" ></td>
  </tr>
  <tr>
  </tr>
  <tr>
    <th>Kewarganegaraan</th>
    <td>:</td>
    <td><input type="text" class="form-control" name="Kewarganegaraan" value="<?php echo $data_KTP[0]['Kewarganegaraan']; ?>" required></td>
  </tr>
  <tr>
    <th>Alamat</th>
    <td>:</td>
    <td><textarea class="form-control" name="Alamat"><?php echo $data_KTP[0]['Alamat']; ?></textarea></td>
  </tr>
  <tr>
    <th>Anak Ke</th>
    <td>:</td>
    <td><input type="number" class="form-control" name="Anak_ke"  value="<?php echo $data_KTP[0]['Anak_ke']; ?>" required></td>
  </tr>
  <tr>
    <th>Status Keluarga</th>
    <td>:</td>
    <td><input type="text" class="form-control" name="Status_keluarga"  value="<?php echo $data_KTP[0]['Status_keluarga']; ?>" required></td>
  </tr>
</table>

<button type="submit" class="btn btn-primary btn-lg">
  <i class="glyphicon glyphicon-floppy-save"></i> Simpan
</button>
</form>

<?php include('../_partials/bottom.php') ?>
