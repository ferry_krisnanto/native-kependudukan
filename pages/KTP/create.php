<?php include('../_partials/top.php') ?>

<h1 class="page-header">Data KTP</h1>
<?php include('_partials/menu.php') ?>

<form action="store.php" method="post">
<table class="table table-striped table-middle">
  <tr>
    <th width="20%">NIK</th>
    <td width="1%">:</td>  
    <td><input type="text" class="form-control" name="NIK" required></td>
  </tr>
  <tr>
    <th width="20%">No KK</th>
    <td width="1%">:</td>  
    <td><input type="text" class="form-control" name="No_KK" required></td>
  </tr>
  <tr>
    <th width="20%">No akta</th>
    <td width="1%">:</td>  
    <td><input type="text" class="form-control" name="No_akta" required></td>
  </tr>
  <tr>
    <th>Nama</th>
    <td>:</td>
    <td><input type="text" class="form-control" name="Nama" required></td>
  </tr>
   <tr>
    <th>Jenis Kelamin</th>
    <td>:</td>
    <td>
      <select class="form-control selectpicker" name="Jenis_kelamin" required>
        <option value="" selected disabled>- pilih -</option>
        <option value="L">Laki-laki</option>
        <option value="P">Perempuan</option>
      </select>
    </td>
  </tr>
  <tr>
    <th>Tempat Lahir</th>
    <td>:</td>
    <td><input type="text" class="form-control" name="Tempat_lahir" required></td>
  </tr>
  <tr>
    <th>Tanggal Lahir</th>
    <td>:</td>
    <td><input type="text" class="form-control datepicker" name="Tanggal_lahir" required></td>
  </tr>
  <tr>
    <th>Status kawin</th>
    <td>:</td>
    <td>
      <select class="form-control selectpicker" name="Status_perkawinan" required>
        <option value="" selected disabled>- pilih -</option>
        <option value="Kawin">Kawin</option>
        <option value="Belum Kawin">Belum Kawin</option>
      </select>
    </td>
  </tr>
  <tr>
    <th width="20%">Agama</th>
    <td width="1%">:</td>
    <td>
      <select class="form-control selectlive" name="Agama" required>
        <option value="" selected disabled>- pilih -</option>
        <option value="Islam">Islam</option>
        <option value="Kristen">Kristen</option>
        <option value="Katholik">Katholik</option>
        <option value="Hindu">Hindu</option>
        <option value="Budha">Budha</option>
        <option value="Konghucu">Konghucu</option>
      </select>
    </td>
  </tr>
  <tr>
    <th>Pendidikan</th>
    <td>:</td>
    <td>
      <select class="form-control selectlive" name="Pendidikan" required>
        <option value="" selected disabled>- pilih -</option>
        <option value="Tidak Sekolah">Tidak Sekolah</option>
        <option value="Tidak Tamat SD">Tidak Tamat SD</option>
        <option value="SD">SD</option>
        <option value="SMP">SMP</option>
        <option value="SMA">SMA</option>
        <option value="D1">D1</option>
        <option value="D2">D2</option>
        <option value="D3">D3</option>
        <option value="S1">S1</option>
        <option value="S2">S2</option>
        <option value="S3">S3</option>
      </select>
    </td>
  </tr>
  <tr>
    <th>Pekerjaan</th>
    <td>:</td>
    <td><input type="text" class="form-control" name="Pekerjaan"></td>
  </tr>
  <tr>
  </tr>
  <tr>
    <th>Kewarganegaraan</th>
    <td>:</td>
    <td><input type="text" class="form-control" name="Kewarganegaraan"></td>
  </tr>
  <tr>
    <th width="20%">Alamat</th>
    <td width="1%">:</td>
    <td><textarea class="form-control" name="Alamat" required></textarea></td>
  </tr>
  <tr>
    <th>Anak Ke</th>
    <td>:</td>
    <td><input type="text" class="form-control" name="Anak_ke"></td>
  </tr>
  <tr>
    <th>Status Keluarga</th>
    <td>:</td>
    <td><input type="text" class="form-control" name="Status_keluarga"></td>
  </tr>
</table>

<button type="submit" class="btn btn-primary btn-lg">
  <i class="glyphicon glyphicon-floppy-save"></i> Simpan
</button>
</form>

<?php include('../_partials/bottom.php') ?>
