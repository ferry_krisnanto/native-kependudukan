<?php
session_start();
if (!isset($_SESSION['user'])) {
  // jika user belum login
  header('Location: ../login');
  exit();
}

include('../../config/koneksi.php');
include('../_partials/uuid.php');

// ambil data dari form
$NIK = htmlspecialchars($_POST['NIK']); //
$No_KK = htmlspecialchars($_POST['No_KK']);
$No_akta = htmlspecialchars($_POST['No_akta']);
$Nama = htmlspecialchars($_POST['Nama']); 
$Jenis_kelamin = htmlspecialchars($_POST['Jenis_kelamin']); 
$Tempat_lahir = htmlspecialchars($_POST['Tempat_lahir']); 
$Tanggal_lahir = htmlspecialchars($_POST['Tanggal_lahir']); 

$Status_perkawinan = htmlspecialchars($_POST['Status_perkawinan']); 
$Agama = htmlspecialchars($_POST['Agama']); 
$Pendidikan = htmlspecialchars($_POST['Pendidikan']); //
$Pekerjaan = htmlspecialchars($_POST['Pekerjaan']);
$Kewarganegaraan = htmlspecialchars($_POST['Kewarganegaraan']);
$Alamat = htmlspecialchars($_POST['Alamat']);
$Anak_ke = htmlspecialchars($_POST['Anak_ke']);
$Status_keluarga = htmlspecialchars($_POST['Status_keluarga']);


$id_user = $_SESSION['user']['id_user'];
$uuid = gen_uuid();

// masukkan ke database
$query = "INSERT INTO Penduduk (uuid, NIK, No_KK, No_akta, Nama, Jenis_kelamin, Tempat_lahir, Tanggal_lahir, Status_perkawinan, Agama, Pendidikan, Pekerjaan, Kewarganegaraan, Alamat, Anak_ke, Status_keluarga) VALUES ( '$uuid', '$NIK', '$No_KK', '$No_akta', '$Nama', '$Jenis_kelamin', '$Tempat_lahir', '$Tanggal_lahir', '$Status_perkawinan', '$Agama', '$Pendidikan', '$Pekerjaan', '$Kewarganegaraan', '$Alamat', '$Anak_ke', '$Status_keluarga' );";

$hasil = mysqli_query($db, $query);

// cek keberhasilan pendambahan data
if ($hasil == true) {
  echo "<script>window.alert('Tambah penduduk berhasil'); window.location.href='../KTP/index.php'</script>";
} else {
  echo "<script>window.alert('Tambah penduduk gagal!'); window.location.href='../KTP/create.php'</script>";
}
