<?php include('../_partials/top.php') ?>

<h1 class="page-header">Data KTP</h1>
<?php include('_partials/menu.php') ?>

<?php include('data-index.php') ?>
<?php include('../beranda/data-index.php') ?>

<table class="table table-striped table-condensed table-hover" id="datatable">
  <thead>
    <tr>
      <th>No</th>
      <th>NIK</th>   
      <th>Nama</th>
	  <th>Tempat Lahir</th>
	  <th>Tanggal Lahir</th>
      <th>L/P</th>
      <!-- <th>Lahir</th> -->
      <th>Usia</th>
	  <th>agama</th>
      <th>Status kawin</th>
      <th>Pendidikan</th>
      <th>Pekerjaan</th>
      <th>Alamat</th>
      <th>Kewarganegaraan</th>
      <th>Aksi</th>
    </tr>
  </thead>
  <tbody>
    <?php $nomor = 1; ?>
    <?php foreach ($data_KTP as $KTP) : ?>
    <tr>
      <td><?php echo $nomor++ ?>.</td>
      <td><?php echo $KTP['NIK'] ?></td>
      <td><?php echo $KTP['Nama'] ?></td>
      <td><?php echo $KTP['Tempat_lahir'] ?></td>
      <td><?php echo $KTP['Tanggal_lahir'] ?></td>
      <!-- <td>
        <?php echo ($KTP['Tanggal_lahir'] != '0000-00-00') ? date('d-m-Y', strtotime($KTP['Tanggal_lahir'])) : ''?>
      </td> -->
      <td><?php echo $KTP['Jenis_kelamin'] ?></td>
      <td><?php echo $KTP['usia'] ?></td>
      <td><?php echo $KTP['Agama'] ?></td>
      <td><?php echo $KTP['Status_perkawinan'] ?></td>
      <td><?php echo $KTP['Pendidikan'] ?></td>
      <td><?php echo $KTP['Pekerjaan'] ?></td>
      <td><?php echo $KTP['Alamat'] ?></td>
      <td><?php echo $KTP['Kewarganegaraan'] ?></td>
      <td>
        <!-- Single button -->
        <div class="btn-group pull-right">
          <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
          <span class="caret"></span>
          </button>
          <ul class="dropdown-menu pull-right" role="menu">
            <li>
              <a href="show.php?uuid=<?php echo $KTP['uuid'] ?>"><i class="glyphicon glyphicon-sunglasses"></i> Detail</a>
            </li>
            <li>
              <a href="cetak-show.php?uuid=<?php echo $KTP['uuid'] ?>" target="_blank"><i class="glyphicon glyphicon-print"></i> Cetak</a>
            </li>
            <?php if ($_SESSION['user']['status'] == 'admin'): ?>
            <li class="divider"></li>
            <li>
              <a href="edit.php?uuid=<?php echo $KTP['uuid'] ?>"><i class="glyphicon glyphicon-edit"></i> edit</a>
            </li>
            <li class="divider"></li>
            <li>
              <a href="delete.php?uuid=<?php echo $KTP['uuid'] ?>" onclick="return confirm('Yakin hapus data ini?')">
                <i class="glyphicon glyphicon-trash"></i> Hapus
              </a>
            </li>
            <?php endif; ?>
          </ul>
        </div>
      </td>
    </tr>
    <?php endforeach ?>
  </tbody>
</table>

<br><br>

<div class="well">
  <dl class="dl-horizontal">
    <dt>Total Penduduk</dt>
    <dd><?php echo $jumlah_Penduduk['total'] ?> orang</dd>

    <dt>Jumlah Laki-laki</dt>
    <dd><?php echo $Jumlah_Penduduk_l['total'] ?> orang</dd>

    <dt>Jumlah Perempuan</dt>
    <dd><?php echo $Jumlah_Penduduk_p['total'] ?> orang</dd>

    <dt>Penduduk < 17 tahun</dt>
    <dd><?php echo $Jumlah_Penduduk_kd_17['total'] ?> orang</dd>

    <dt>Penduduk >= 17 tahun</dt>
    <dd><?php echo $Jumlah_Penduduk_ld_17['total'] ?> orang</dd>
  </dl>
</div>

<?php include('../_partials/bottom.php') ?>
