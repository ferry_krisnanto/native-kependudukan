<?php include('../_partials/top.php') ?>

<h1 class="page-header">Data KTP</h1>
<?php include('_partials/menu.php') ?>

<?php include('data-show.php') ?>

<table class="table table-striped">
  <tr>
    <th width="20%">NIK</th>
    <td width="1%">:</td>
    <td><?php echo $data_KTP[0]['NIK'] ?></td>
  </tr> 
  <tr>
    <th width="20%">No KK</th>
    <td width="1%">:</td>
    <td><?php echo $data_KTP[0]['No_KK'] ?></td>
  </tr> 
  <tr>
    <th width="20%">No akta</th>
    <td width="1%">:</td>
    <td><?php echo $data_KTP[0]['No_akta'] ?></td>
  </tr> 
  <tr>
    <th>Nama </th>
    <td>:</td>
    <td><?php echo $data_KTP[0]['Nama'] ?></td>
  </tr>
  <tr>
    <th>Jenis Kelamin</th>
    <td>:</td>
    <td><?php echo $data_KTP[0]['Jenis_kelamin'] ?></td>
  </tr>
  <tr>
    <th>Tempat Lahir</th>
    <td>:</td>
    <td><?php echo $data_KTP[0]['Tempat_lahir'] ?></td>
  </tr>
  <tr>
    <th>Tanggal Lahir</th>
    <td>:</td>
    <td>
      <?php echo ($data_KTP[0]['Tanggal_lahir'] != '0000-00-00') ? date('d-m-Y', strtotime($data_KTP[0]['Tanggal_lahir'])) : ''?>
    </td>
  </tr>
  <tr>
    <th>Status kawin</th>
    <td>:</td>
    <td><?php echo $data_KTP[0]['Status_perkawinan'] ?></td>
  </tr>
  <tr>
    <th width="20%">Agama</th>
    <td width="1%">:</td>
    <td><?php echo $data_KTP[0]['Agama'] ?></td>
  </tr>
  <tr>
    <th>Pendidikan</th>
    <td>:</td>
    <td><?php echo $data_KTP[0]['Pendidikan'] ?></td>
  </tr>
  <tr>
    <th>Pekerjaan</th>
    <td>:</td>
    <td><?php echo $data_KTP[0]['Pekerjaan'] ?></td>
  </tr>
  <tr>
    <th>Kewarganegaraan</th>
    <td>:</td>
    <td><?php echo $data_KTP[0]['Kewarganegaraan'] ?></td>
  </tr>
  
  <tr>
    <th width="20%">Alamat</th>
    <td width="1%">:</td>
    <td><?php echo $data_KTP[0]['Alamat'] ?></td>
  </tr>
  <tr>
    <th>Anak Ke</th>
    <td>:</td>
    <td><?php echo $data_KTP[0]['Anak_ke'] ?></td>
  </tr>
  <tr>
    <th>Keterangan Pindah</th>
    <td>:</td>
    <td><?php echo $data_KTP[0]['Keterangan_pindah'] ?></td>
  </tr>
  <tr>
    <th>Status Keluarga</th>
    <td>:</td>
    <td><?php echo $data_KTP[0]['Status_keluarga'] ?></td>
  </tr>
</table>

<?php include('../_partials/bottom.php') ?>
