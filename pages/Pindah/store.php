<?php
session_start();
if (!isset($_SESSION['user'])) {
  // jika user belum login
  header('Location: ../login');
  exit();
}

include('../../config/koneksi.php');
include('../_partials/uuid.php');

// ambil data dari form
$NIK = htmlspecialchars($_POST['NIK']);
$Nama = htmlspecialchars($_POST['Nama']);
$Jenis_kelamin = htmlspecialchars($_POST['Jenis_kelamin']);

$Pekerjaan = htmlspecialchars($_POST['Pekerjaan']);
$Alamat = htmlspecialchars($_POST['Alamat']);
$Alamat_Baru = htmlspecialchars($_POST['Alamat_Baru']);
$Tanggal_Pindah = htmlspecialchars($_POST['Tanggal_Pindah']);
$uuid = gen_uuid();

// masukkan ke database

$query = "INSERT INTO penduduk ( uuid, NIK, Nama, Jenis_kelamin, Pekerjaan, Alamat) VALUES ( '$uuid', '$NIK', '$Nama', '$Jenis_kelamin', '$Pekerjaan', '$Alamat');";
$hasil = mysqli_query($db, $query);
//echo json_decode ($hasil);

if($hasil){
	$query2	= "INSERT INTO pindah ( NIK, Alamat_Baru, Tanggal_Pindah) VALUES ( '$NIK', '$Alamat_Baru', '$Tanggal_Pindah');";
	$end 	= mysqli_query($db, $query2);
}

// cek keberhasilan pendambahan data
if ($end) {
  echo "<script>window.alert('Pindah Penduduk Berhasil'); window.location.href='../Pindah/'</script>";
} else {
  echo "<script>window.alert('Pindah Penduduk Gagal!'); window.location.href='../Pindah/'</script>";
}
