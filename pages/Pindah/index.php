<?php include('../_partials/top.php') ?>

<h1 class="page-header">Data Pindah</h1>
<?php include('_partials/menu.php') ?>

<?php include('data-index.php') ?>
<?php include('../Beranda/data-index.php') ?>

<table class="table table-striped table-condensed table-hover" id="datatable">
  <thead>
    <tr>
      <th>No</th>
      <th>NIK</th>
      <th>Nama</th>
      <th>L/P</th>
      <th>Pekerjaan</th>
      <th>Alamat Lama</th>
      <th>Alamat Baru</th>
      <th>Tanggal Pindah</th>
      <th>Aksi</th>
    </tr>
  </thead>
  <tbody>
    <?php $nomor = 1; ?>
    <?php foreach ($data_Pindah as $Pindah) : ?>
    <tr>
      <td><?php echo $nomor++ ?>.</td>
      <td><?php echo $Pindah['NIK'] ?></td>
      <td><?php echo $Pindah['Nama'] ?></td>
      <td><?php echo $Pindah['Jenis_kelamin'] ?></td>
      <td><?php echo $Pindah['Pekerjaan'] ?></td>
      <td><?php echo $Pindah['Alamat'] ?></td>
      <td><?php echo $Pindah['Alamat_Baru'] ?></td>
      <td><?php echo $Pindah['Tanggal_Pindah'] ?></td>
      <td>
        <!-- Single button -->
        <div class="btn-group pull-right">
          <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
          <span class="caret"></span>
          </button>
          <ul class="dropdown-menu pull-right" role="menu">
            <li>
              <a href="show.php?NIK=<?php echo $Pindah['NIK'] ?>"><i class="glyphicon glyphicon-sunglasses"></i> Detail</a>
            </li>
            <li>
              <a href="cetak-show.php?NIK=<?php echo $Pindah['NIK'] ?>" target="_blank"><i class="glyphicon glyphicon-print"></i> Cetak</a>
            </li>
            <?php if ($_SESSION['user']['status'] == 'admin'): ?>
            <li class="divider"></li>
            <li>
              <a href="edit.php?NIK=<?php echo $Pindah['NIK'] ?>"><i class="glyphicon glyphicon-edit"></i> edit</a>
            </li>
            <li class="divider"></li>
            <li>
              <a href="delete.php?NIK=<?php echo $Pindah['NIK'] ?>" onclick="return confirm('Yakin hapus data ini?')">
                <i class="glyphicon glyphicon-trash"></i> Hapus
              </a>
            </li>
            <?php endif; ?>
          </ul>
        </div>
      </td>
    </tr>
    <?php endforeach ?>
  </tbody>
</table>

<br><br>

<div class="well">
  <dl class="dl-horizontal">
    <dt>Total Pindah</dt>
    <dd><?php echo $Jumlah_Pindah['total'] ?> orang</dd>

    <dt>Jumlah Laki-laki</dt>
    <dd><?php echo $Jumlah_Pindah_l['total'] ?> orang</dd>

    <dt>Jumlah Perempuan</dt>
    <dd><?php echo $Jumlah_Pindah_p['total'] ?> orang</dd>

    <dt>Penduduk < 17 tahun</dt>
    <dd><?php echo $Jumlah_Pindah_kd_17['total'] ?> orang</dd>

    <dt>Penduduk >= 17 tahun</dt>
    <dd><?php echo $Jumlah_Pindah_ld_17['total'] ?> orang</dd>
  </dl>
</div>

<?php include('../_partials/bottom.php') ?>
