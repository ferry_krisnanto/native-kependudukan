<?php include('../_partials/top.php') ?>

<h1 class="page-header">Data Pindah</h1>
<?php include('_partials/menu.php') ?>

<form action="store.php" method="post">
<table class="table table-striped table-middle">
  <tr>
    <th width="20%">NIK</th>
    <td width="1%">:</td>
    <td><input type="text" class="form-control" name="NIK" required></td>
  </tr>
  <tr>
    <th>Nama</th>
    <td>:</td>
    <td><input type="text" class="form-control" name="Nama" required></td>
  </tr>
   <tr>
    <th>Jenis Kelamin</th>
    <td>:</td>
    <td>
      <select class="form-control selectpicker" name="Jenis_kelamin" required>
        <option value="" selected disabled>- pilih -</option>
        <option value="L">Laki-laki</option>
        <option value="P">Perempuan</option>
      </select>
    </td>
  </tr>
  <tr>
    <th>Pekerjaan</th>
    <td>:</td>
    <td><input type="text" class="form-control" name="Pekerjaan" required></td>
  </tr>
  <tr>
    <th width="20%">Alamat Lama</th>
    <td width="1%">:</td>
    <td><input type="text" class="form-control" name="Alamat" required></td>
  </tr>
  <tr>
    <th width="20%">Alamat Baru</th>
    <td width="1%">:</td>
    <td><input type="text" class="form-control" name="Alamat_Baru" required></td>
  </tr>
  <tr>
    <th>Tanggal Pindah</th>
    <td>:</td>
    <td><input type="text" class="form-control datapicker" name="Tanggal_Pindah"required></td>
  </tr>
</table>

<button type="submit" class="btn btn-primary btn-lg">
  <i class="glyphicon glyphicon-export"></i> Simpan
</button>
</form>

<?php include('../_partials/bottom.php') ?>

