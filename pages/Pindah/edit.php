<?php include('../_partials/top.php') ?>

<h1 class="page-header">Data Pindah</h1>
<?php include('_partials/menu.php') ?>


<?php include('data-show.php') ?>

<form action="Update.php" method="post">
<table class="table table-striped table-middle">
 <tr>
    <th width="20%">NIK</th>
    <td width="1%">:</td>
    <td><input type="text" class="form-control" name="NIK" value="<?php echo $data_Pindah[0]['NIK']; ?>" required></td>
  </tr>
  <tr>
    <th>Nama</th>
    <td>:</td>
    <td><input type="text" class="form-control" name="Nama" value="<?php echo $data_Pindah[0]['Nama']; ?>"required></td>
  </tr>
   <tr>
    <th>Jenis Kelamin</th>
    <td>:</td>
    <td>
      <select class="form-control selectpicker" name="Jenis_kelamin" readonly>
	  value="<?php echo $data_Pindah[0]['Jenis_kelamin']; ?>"
	  <?php
		if($data_Pindah[0]['Jenis_kelamin'] == 'L'){
	  ?>
		<option value="L">Laki-laki</option>
        <option value="P">Perempuan</option>
	  <?php
		}else{
	  ?>
        <option value="P">Perempuan</option>
		<option value="L">Laki-laki</option>
	  <?php
		}
	  ?>
      </select>
    </td>
  </tr>
  <tr>
    <th>Pekerjaan</th>
    <td>:</td>
    <td><input type="text" class="form-control" name="Pekerjaan" value="<?php echo $data_Pindah[0]['Pekerjaan'] ?>" required></td>
  </tr>
  <tr>
    <th width="20%">Alamat Lama</th>
    <td width="1%">:</td>
    <td><textarea class="form-control" name="Alamat"><?php echo $data_Pindah[0]['Alamat']; ?></textarea></td>
  </tr>
  <tr>
    <th width="20%">Alamat Baru</th>
    <td width="1%">:</td>
    <td><textarea class="form-control" name="Alamat_Baru"><?php echo $data_Pindah[0]['Alamat_Baru']; ?></textarea></td>
  </tr>
  <tr>
    <th>Tanggal Pindah</th>
    <td>:</td>
    <td><input type="text" class="form-control datepicker" name="Tanggal_Pindah" value="<?php echo $data_Pindah[0]['Tanggal_Pindah'] ?>"  required></td>
  </tr>
</table>

<button type="submit" class="btn btn-primary btn-lg">
  <i class="glyphicon glyphicon-floppy-save"></i> Simpan
</button>
</form>

<?php include('../_partials/bottom.php') ?>
