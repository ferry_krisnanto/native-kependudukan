<?php include('../_partials/top.php') ?>

<h1 class="page-header">Data Pindah</h1>
<?php include('_partials/menu.php') ?>

<?php include('data-show.php') ?>

<table class="table table-striped">
  <tr>
    <th width="20%">NIK</th>
    <td width="1%">:</td>
    <td><?php echo $data_Pindah[0]['NIK'] ?></td>
  </tr>
  <tr>
    <th>Nama</th>
    <td>:</td>
    <td><?php echo $data_Pindah[0]['Nama'] ?></td>
  </tr>
  <tr>
    <th>Jenis Kelamin</th>
    <td>:</td>
    <td><?php echo $data_Pindah[0]['Jenis_kelamin'] ?></td>
  </tr>
  <tr>
    <th>Pekerjaan</th>
    <td>:</td>
    <td><?php echo $data_Pindah[0]['Pekerjaan'] ?></td>
  </tr>
  <tr>
    <th width="20%">Alamat Lama</th>
    <td width="1%">:</td>
    <td><?php echo $data_Pindah[0]['Alamat'] ?></td>
  </tr>
  <tr>
    <th width="20%">Alamat Baru</th>
    <td width="1%">:</td>
    <td><?php echo $data_Pindah[0]['Alamat_Baru'] ?></td>
  </tr>
  <tr>
    <th>Tanggal Pindah</th>
    <td>:</td>
    <td><?php echo $data_Pindah[0]['Tanggal_Pindah'] ?></td>
  </tr>
</table>

<?php include('../_partials/bottom.php') ?>
