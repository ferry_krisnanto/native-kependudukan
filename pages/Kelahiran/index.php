<?php include('../_partials/top.php') ?>

<h1 class="page-header">Data Kelahiran</h1>
<?php include('_partials/menu.php') ?>

<?php include('data-index.php') ?>
<?php include('../Beranda/data-index.php') ?>

<table class="table table-striped table-condensed table-hover" id="datatable">
  <thead>
    <tr>
      <th>No</th>
      <th>No Akta</th>
      <th>No KK</th>
      <th>Nama</th>
      <th>Tempat Lahir</th>
      <th>Tanggal Lahir</th>
      <th>L/P</th>
      <th>Anak Ke</th>
      <th>Penolong</th>
      <th>Aksi</th>
    </tr>
  </thead>
  <tbody>
    <?php $nomor = 1; ?>
    <?php foreach ($data_Kelahiran as $Kelahiran) : ?>
    <tr>
      <td><?php echo $nomor++ ?>.</td>
      <td><?php echo $Kelahiran['No_akta'] ?></td>
      <td><?php echo $Kelahiran['No_KK'] ?></td>
      <td><?php echo $Kelahiran['Nama'] ?></td>
      <td><?php echo $Kelahiran['Tempat_lahir'] ?></td>
      <td><?php echo $Kelahiran['Tanggal_lahir'] ?></td>
      <td><?php echo $Kelahiran['Jenis_kelamin'] ?></td>
      <td><?php echo $Kelahiran['Anak_ke'] ?></td>
      <td><?php echo $Kelahiran['Penolong'] ?></td>
      <td>
        <!-- Single button -->
        <div class="btn-group pull-right">
          <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
          <span class="caret"></span>
          </button>
          <ul class="dropdown-menu pull-right" role="menu">
            <li>
              <a href="show.php?No_akta=<?php echo $Kelahiran['No_akta'] ?>"><i class="glyphicon glyphicon-sunglasses"></i> Detail</a>
            </li>
            <li>
              <a href="cetak-show.php?No_akta=<?php echo $Kelahiran['No_akta'] ?>" target="_blank"><i class="glyphicon glyphicon-print"></i> Cetak</a>
            </li>
            <?php if ($_SESSION['user']['status'] == 'admin'): ?>
            <li class="divider"></li>
            <li>
              <a href="edit.php?No_akta=<?php echo $Kelahiran['No_akta'] ?>"><i class="glyphicon glyphicon-edit"></i> edit</a>
            </li>
            <li class="divider"></li>
            <li>
              <a href="delete.php?No_akta=<?php echo $Kelahiran['No_akta'] ?>" onclick="return confirm('Yakin hapus data ini?')">
                <i class="glyphicon glyphicon-trash"></i> Hapus
              </a>
            </li>
            <?php endif; ?>
          </ul>
        </div>
      </td>
    </tr>
    <?php endforeach ?>
  </tbody>
</table>
<br><br>


<?php include('../_partials/bottom.php') ?>
