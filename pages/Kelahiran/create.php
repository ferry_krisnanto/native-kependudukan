<?php include('../_partials/top.php') ?>

<h1 class="page-header">Data Kelahiran</h1>
<?php include('_partials/menu.php') ?>

<form action="store.php" method="post">
<table class="table table-striped table-middle">
  <tr>
    <th width="20%">No akta</th>
    <td width="1%">:</td>
    <td><input type="text" class="form-control" name="No_akta" required></td>
  </tr>
  <tr>
    <th width="20%">No KK</th>
    <td width="1%">:</td>
    <td><input type="text" class="form-control" name="No_KK" required></td>
  </tr>
  <tr>
    <th>Nama</th>
    <td>:</td>
    <td><input type="text" class="form-control" name="Nama" required></td>
  </tr>
  <tr>
    <th>Tempat Lahir</th>
    <td>:</td>
    <td><input type="text" class="form-control" name="Tempat_lahir" required></td>
  </tr>
  <tr>
    <th>Tanggal Lahir</th>
    <td>:</td>
    <td><input type="text" class="form-control datepicker" name="Tanggal_lahir" required></td>
  </tr>
   <tr>
    <th>Jenis Kelamin</th>
    <td>:</td>
    <td>
      <select class="form-control selectpicker" name="Jenis_kelamin" required>
        <option value="" selected disabled>- pilih -</option>
        <option value="L">Laki-laki</option>
        <option value="P">Perempuan</option>
      </select>
    </td>
  </tr>
  <tr>
    <th>Anak Ke</th>
    <td>:</td>
    <td><input type="text" class="form-control" name="Anak_ke"></td>
  </tr>
  <tr>
    <th>Penolong</th>
    <td>:</td>
    <td><input type="text" class="form-control" name="Penolong"></td>
  </tr>
</table>

<button type="submit" class="btn btn-primary btn-lg">
  <i class="glyphicon glyphicon-export"></i> Simpan
</button>
</form>

<?php include('../_partials/bottom.php') ?>
