<?php include('../_partials/top.php') ?>

<h1 class="page-header">Data Kelahiran</h1>
<?php include('_partials/menu.php') ?>

<?php include('data-show.php') ?>

<table class="table table-striped">
  <tr>
    <th width="20%">No akta</th>
    <td width="1%">:</td>
    <td><?php echo $data_kelahiran[0]['No_akta'] ?></td>
  </tr>
  <tr>
    <th width="20%">No KK</th>
    <td width="1%">:</td>
    <td><?php echo $data_kelahiran[0]['No_KK'] ?></td>
  </tr>
  <tr>
    <th>Nama</th>
    <td>:</td>
    <td><?php echo $data_kelahiran[0]['Nama'] ?></td>
  </tr>
  <tr>
    <th>Tempat lahir</th>
    <td>:</td>
    <td><?php echo $data_kelahiran[0]['Tempat_lahir'] ?></td>
  </tr>
  <tr>
    <th>Tanggal lahir</th>
    <td>:</td>
    <td><?php echo $data_kelahiran[0]['Tanggal_lahir'] ?></td>
  </tr>
  <tr>
    <th>Jenis Kelamin</th>
    <td>:</td>
    <td><?php echo $data_kelahiran[0]['Jenis_kelamin'] ?></td>
  </tr>
  <tr>
    <th>Anak Ke</th>
    <td>:</td>
    <td><?php echo $data_kelahiran[0]['Anak_ke'] ?></td>
  </tr>
  <tr>
    <th>Penolong</th>
    <td>:</td>
    <td><?php echo $data_kelahiran[0]['Penolong'] ?></td>
  </tr>
</table>

<?php include('../_partials/bottom.php') ?>
