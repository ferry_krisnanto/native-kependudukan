<?php include('../_partials/top.php') ?>

<h1 class="page-header">Data Kelahiran</h1>
<?php include('_partials/menu.php') ?>

<?php include('../Kelahiran/data-show.php') ?>

<form action="update.php" method="post">
<table class="table table-striped table-middle">
  <tr>
    <th width="20%">No akta</th>
    <td width="1%">:</td>  
    <td><input type="text" class="form-control" name="No_akta" value="<?php echo $data_kelahiran[0]['No_akta'] ?>"></td>
  </tr>
  <tr>
    <th width="20%">No KK</th>
    <td width="1%">:</td>  
    <td><input type="text" class="form-control" name="No_KK" value="<?php echo $data_kelahiran[0]['No_KK'] ?>" required></td>
  </tr>
  <tr>
    <th>Nama</th>
    <td>:</td>
    <td><input type="text" class="form-control" name="Nama" value="<?php echo $data_kelahiran[0]['Nama'] ?>"required></td>
  </tr>
  <tr>
    <th>Tempat Lahir</th>
    <td>:</td>
    <td><input type="text" class="form-control" name="Tempat_lahir" value="<?php echo $data_kelahiran[0]['Tempat_lahir'] ?>"required></td>
  </tr>
  <tr>
    <th>Tanggal Lahir</th>
    <td>:</td>
    <td><input type="text" class="form-control datepicker" name="Tanggal_lahir" value="<?php echo $data_kelahiran[0]['Tanggal_lahir'] ?>"required></td>
  </tr>
   <tr>
    <th>Jenis Kelamin</th>
    <td>:</td>
    <td>
      <select class="form-control selectpicker" name="Jenis_kelamin" required>
	  value="<?php echo $data_kelahiran[0]['Jenis_kelamin'] ?>"
	  <?php
		if($data_Kelahiran[0]['Jenis_kelamin'] == 'L'){
	  ?>
		<option value="L">Laki-laki</option>
        <option value="P">Perempuan</option>
	  <?php
		}else{
	  ?>
        <option value="P">Perempuan</option>
		<option value="L">Laki-laki</option>
	  <?php
		}
	  ?>
      </select>
    </td>
  </tr>
  <tr>
    <th>Anak Ke</th>
    <td>:</td>
    <td><input type="text" class="form-control" name="Anak_ke" value="<?php echo $data_kelahiran[0]['Anak_ke'] ?>"required></td>
  </tr>
  <tr>
    <th>Penolong</th>
    <td>:</td>
    <td><input type="text" class="form-control" name="Penolong" value="<?php echo $data_kelahiran[0]['Penolong'] ?>"required></td>
  </tr>
</table>

<button type="submit" class="btn btn-primary btn-lg">
  <i class="glyphicon glyphicon-export"></i> Simpan
</button>
</form>

<?php include('../_partials/bottom.php') ?>
