<?php include('../_partials/top.php') ?>

<h1 class="page-header">Beranda</h1>

<?php include('data-index.php') ?>

<div class="row">
  <div class="col-sm-6 col-md-4">
    <div class="panel panel-primary">
      <div class="panel-body">
        <h3>Data Penduduk</h3>
        <p>
          Total ada <?php echo $jumlah_Penduduk['total'] ?> data Penduduk. <?php echo $Jumlah_Penduduk_l['total'] ?> di antaranya laki-laki, dan <?php echo $Jumlah_Penduduk_p['total'] ?> diantaranya perempuan.
        </p>
        <p>
           Penduduk di atas 17 tahun berjumlah <?php echo $Jumlah_Penduduk_ld_17['total'] ?> orang, dan di bawah 17 tahun berjumlah <?php echo $Jumlah_Penduduk_kd_17['total'] ?> orang.
        </p>
      </div>
      <div class="panel-footer">
        <a href="../Data KTP" class="btn btn-primary" role="button">
          <span class="glyphicon glyphicon-book"></span> Detil »
        </a>
      </div>
    </div>
  </div>

  <div class="col-sm-6 col-md-4">
    <div class="panel panel-primary">
      <div class="panel-body">
        <h3>Data Kartu Keluarga</h3>
        <p>Total ada <?php echo $jumlah_kartu_keluarga['total'] ?> data kartu keluarga</p>
      </div>
      <div class="panel-footer">
        <a href="../kartu-keluarga" class="btn btn-primary" role="button">
          <span class="glyphicon glyphicon-inbox"></span> Detil »
        </a>
      </div>
    </div>
  </div>

  <div class="col-sm-6 col-md-4">
    <div class="panel panel-primary">
      <div class="panel-body">
        <h3>Data Pindah</h3>
        <p>
          Total ada <?php echo $Jumlah_Pindah['total'] ?> data Pindah. <?php echo $Jumlah_Pindah_l['total'] ?> di antaranya laki-laki, dan <?php echo $Jumlah_Pindah_p['total'] ?> diantaranya perempuan.
        </p>
        <p>
           Penduduk di atas 17 tahun berjumlah <?php echo $Jumlah_Pindah_ld_17['total'] ?> orang, dan di bawah 17 tahun berjumlah <?php echo $Jumlah_Pindah_kd_17['total'] ?> orang.
        </p>
      </div>
      <div class="panel-footer">
        <a href="../Pindah" class="btn btn-primary" role="button">
          <span class="glyphicon glyphicon-export"></span> Detil »
        </a>
      </div>
    </div>
  </div>
</div>

<?php include('../_partials/bottom.php') ?>
