<?php
include('../../config/koneksi.php');

// hitung KTP
$query_KTP = "SELECT COUNT(*) AS total FROM Penduduk";
$hasil_KTP = mysqli_query($db, $query_KTP);
$jumlah_Penduduk = mysqli_fetch_assoc($hasil_KTP);

// hitung kartu keluarga
$query_kartu_keluarga = "SELECT COUNT(*) AS total FROM kk";
$hasil_kartu_keluarga = mysqli_query($db, $query_kartu_keluarga);
$jumlah_kartu_keluarga = mysqli_fetch_assoc($hasil_kartu_keluarga);

// hitung KTP laki-laki
$query_KTP_l = "SELECT COUNT(*) AS total FROM Penduduk WHERE Jenis_kelamin = 'L'";
$hasil_KTP_l = mysqli_query($db, $query_KTP_l);
$Jumlah_Penduduk_l = mysqli_fetch_assoc($hasil_KTP_l);

// hitung KTP perempuan
$query_KTP_p = "SELECT COUNT(*) AS total FROM Penduduk WHERE Jenis_kelamin = 'P'";
$hasil_KTP_p = mysqli_query($db, $query_KTP_p);
$Jumlah_Penduduk_p = mysqli_fetch_assoc($hasil_KTP_p);

// hitung KTP lebih dari 17 tahun
$query_KTP_ld_17 = "SELECT COUNT(*) AS total FROM Penduduk WHERE TIMESTAMPDIFF(YEAR, Tanggal_lahir, CURDATE()) >= 17 AND Tanggal_lahir != '0000-00-00'";
$hasil_KTP_ld_17 = mysqli_query($db, $query_KTP_ld_17);
$Jumlah_Penduduk_ld_17 = mysqli_fetch_assoc($hasil_KTP_ld_17);

// hitung KTP kurang dari 17 tahun
$query_KTP_kd_17 = "SELECT COUNT(*) AS total FROM Penduduk WHERE TIMESTAMPDIFF(YEAR, Tanggal_lahir, CURDATE()) < 17 AND Tanggal_lahir != '0000-00-00'";
$hasil_KTP_kd_17 = mysqli_query($db, $query_KTP_kd_17);
$Jumlah_Penduduk_kd_17 = mysqli_fetch_assoc($hasil_KTP_kd_17);

// hitung Pindah
$query_Pindah = "SELECT COUNT(*) AS total FROM Pindah";
$hasil_Pindah = mysqli_query($db, $query_Pindah);
$Jumlah_Pindah = mysqli_fetch_assoc($hasil_Pindah);


// hitung Pindah laki-laki
$query_Pindah_l = "SELECT COUNT(*) AS total FROM Penduduk WHERE Jenis_kelamin = 'L'";
$hasil_Pindah_l = mysqli_query($db, $query_Pindah_l);
$Jumlah_Pindah_l = mysqli_fetch_assoc($hasil_Pindah_l);

// hitung Pindah Perempuan
$query_Pindah_p = "SELECT COUNT(*) AS total FROM Penduduk WHERE Jenis_kelamin = 'P'";
$hasil_Pindah_p = mysqli_query($db, $query_Pindah_p);
$Jumlah_Pindah_p = mysqli_fetch_assoc($hasil_Pindah_p);

// hitung Pindah lebih dari 17 tahun
$query_Pindah_ld_17 = "SELECT COUNT(*) AS total FROM Penduduk WHERE TIMESTAMPDIFF(YEAR, Tanggal_lahir, CURDATE()) >= 17 AND Tanggal_lahir != '0000-00-00'";
$hasil_Pindah_ld_17 = mysqli_query($db, $query_Pindah_ld_17);
$Jumlah_Pindah_ld_17 = mysqli_fetch_assoc($hasil_Pindah_ld_17);

// hitung Pindah kurang dari 17 tahun
$query_Pindah_kd_17 = "SELECT COUNT(*) AS total FROM Penduduk WHERE TIMESTAMPDIFF(YEAR, Tanggal_lahir, CURDATE()) < 17 AND Tanggal_lahir != '0000-00-00'";
$hasil_Pindah_kd_17 = mysqli_query($db, $query_Pindah_kd_17);
$Jumlah_Pindah_kd_17 = mysqli_fetch_assoc($hasil_Pindah_kd_17);
