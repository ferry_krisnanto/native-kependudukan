<?php
include('../../config/koneksi.php');


// hitung Pindah
$query_Pindah = "SELECT COUNT(*) AS total FROM pindah";
$hasil_Pindah = mysqli_query($db, $query_Pindah);
$Jumlah_Pindah = mysqli_fetch_assoc($hasil_Pindah);


// hitung Pindah laki-laki
$query_Pindah_l = "SELECT COUNT(*) AS total FROM pindah WHERE Jenis_kelamin = 'L'";
$hasil_Pindah_l = mysqli_query($db, $query_Pindah_l);
$Jumlah_Pindah_l = mysqli_fetch_assoc($hasil_Pindah_l);

// hitung Pindah Perempuan
$query_Pindah_p = "SELECT COUNT(*) AS total FROM pindah WHERE Jenis_kelamin = 'P'";
$hasil_Pindah_p = mysqli_query($db, $query_Pindah_p);
$Jumlah_Pindah_p = mysqli_fetch_assoc($hasil_Pindah_p);

// hitung Pindah lebih dari 17 tahun
$query_Pindah_ld_17 = "SELECT COUNT(*) AS total FROM pindah WHERE TIMESTAMPDIFF(YEAR, Tanggal_lahir, CURDATE()) >= 17 AND Tanggal_lahir != '0000-00-00'";
$hasil_Pindah_ld_17 = mysqli_query($db, $query_Pindah_ld_17);
$Jumlah_Pindah_ld_17 = mysqli_fetch_assoc($hasil_Pindah_ld_17);

// hitung Pindah kurang dari 17 tahun
$query_Pindah_kd_17 = "SELECT COUNT(*) AS total FROM pindah WHERE TIMESTAMPDIFF(YEAR, Tanggal_lahir, CURDATE()) < 17 AND Tanggal_lahir != '0000-00-00'";
$hasil_Pindah_kd_17 = mysqli_query($db, $query_Pindah_kd_17);
$Jumlah_Pindah_kd_17 = mysqli_fetch_assoc($hasil_Pindah_kd_17);
