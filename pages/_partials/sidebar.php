 <?php
function is_active($page) {
  $uri = "$_SERVER[REQUEST_URI]";
  if (strpos($uri, $page)) {
    echo 'active';
  }
}
?>

<ul class="nav nav-sidebar">
  <li class="<?php is_active('beranda'); ?>">
    <a href="../beranda"><i class="glyphicon glyphicon-home"></i> Beranda</a> 
  </li>
</ul>

<ul class="nav nav-sidebar">
  <li class="<?php is_active('KTP'); ?>">
    <a href="../KTP"><i class="glyphicon glyphicon-book"></i> Data KTP</a>
  </li>
  <li class="<?php is_active('Kelahiran'); ?>">
    <a href="../Kelahiran"><i class="glyphicon glyphicon-plus"></i> Data Kelahiran</a>
  </li>
  <li class="<?php is_active('kartu-keluarga'); ?>">
    <a href="../kartu-keluarga"><i class="glyphicon glyphicon-inbox"></i> Data Kartu Keluarga</a>
  </li>
  <li class="<?php is_active('Pindah'); ?>">
    <a href="../Pindah"><i class="glyphicon glyphicon-export"></i> Data Pindah</a>
  </li>
  <li class="<?php is_active('Kematian'); ?>">
    <a href="../Kematian"><i class="glyphicon glyphicon-minus"></i> Data Kematian</a>
  </li>
  <li class="<?php is_active('Laporan'); ?>">
    <a href="../Laporan"><i class="glyphicon glyphicon-book"></i> Laporan</a>
  </li>
</ul>

<?php if ($_SESSION['user']['status'] == 'admin'): ?>
<ul class="nav nav-sidebar">
  <li class="<?php is_active('user'); ?>">
    <a href="../user"><i class="glyphicon glyphicon-user"></i> User</a>
  </li>
</ul>
<?php endif; ?>
