<?php include('../_partials/top.php') ?>

<h1 class="page-header">Data Kartu Keluarga</h1>
<?php include('_partials/menu.php') ?>

<?php include('data-show.php') ?>

<form action="store.php" method="post">
<table class="table table-striped table-middle">
  <tr>
    <th width="20%">No_KK</th>
    <td width="1%">:</td>
    <td><input type="text" class="form-control" name="No_KK" value="<?php echo $data_keluarga[0]['No_KK'] ?>" required></td> 
  </tr>
  <tr>
    <th>ID Kepala Keluarga</th>
    <td>:</td>
    <td>
      <select class="form-control selectlive" name="Id_Kepala_Keluarga" required>
        <option value="" selected disabled>- pilih -</option>
        <?php foreach ($data_keluarga as $kk) : ?>
        <option value="<?php echo $kk['No_KK'] ?>">
          <?php echo $kk['Nama'] ?> (NIK: <?php echo $kk['No_KK'] ?>)
        </option>
        <?php endforeach ?>
      </select>
    </td>
  </tr>
   <tr>
    <th width="20%">Jumlah Anggota Keluarga</th>
    <td width="1%">:</td>
    <td><input type="text" class="form-control" name="Id_Kepala_Keluarga" value="<?php echo $data_keluarga[0]['Id_Kepala_Keluarga'] ?>" required></td>
  </tr>
  <tr>
   <tr>
    <th width="20%">ID Anggota Keluarga (istri)</th>
    <td width="1%">:</td>
    <td>
      <select class="form-control selectlive" name="Id_Kepala_Keluarga" >
        <option value="" selected disabled>- pilih -</option>
        <?php foreach ($data_kk as $kk) : ?>
        <option value="<?php echo $kk['No_KK'] ?>">
          <?php echo $kk['Nama'] ?> (NIK: <?php echo $kk['No_KK'] ?>)
        </option>
        <?php endforeach ?>
      </select>
    </td>
  </tr>
  <tr>
    <th>ID Anggota Keluarga (Anak)</th>
    <td>:</td>
    <td>
      <select class="form-control selectlive" name="Id_Kepala_Keluarga" >
        <option value="" selected disabled>- pilih -</option>
        <?php foreach ($data_kk as $kk) : ?>
        <option value="<?php echo $kk['No_KK'] ?>">
          <?php echo $kk['Nama'] ?> (NIK: <?php echo $kk['No_KK'] ?>)
        </option>
        <?php endforeach ?>
      </select>
    </td>
  </tr>
  <tr>
    <th>Agama</th>
    <td>:</td>
    <td><input type="text" class="form-control" name="Agama" value="<?php echo $data_keluarga[0]['Agama'] ?>" required></td>
  </tr>
  <tr>
    <th>Pendidikan</th>
    <td>:</td>
    <td><input type="text" class="form-control" name="Pendidikan" value="<?php echo $data_keluarga[0]['Pendidikan'] ?>" required></td>
  </tr>
  <tr>
    <th>Pekerjaan</th>
    <td>:</td>
    <td><input type="text" class="form-control" name="Pekerjaan" value="<?php echo $data_keluarga[0]['Pekerjaan'] ?>" required></td>
  </tr>
  <tr>
    <th width="20%">Alamat</th>
    <td width="1%">:</td>
    <td><textarea class="form-control" name="Alamat" ></textarea></td>
  </tr>
  <tr>
    <th>RT</th>
    <td>:</td>
    <td><input type="text" class="form-control" name="RT" value="<?php echo $data_keluarga[0]['RT'] ?>" required></td>
  </tr>
  <tr>
    <th>RW</th>
    <td>:</td>
    <td><input type="text" class="form-control" name="RW"value="<?php echo $data_keluarga[0]['RW'] ?>" required></td>
  </tr>
  <tr>
    <th>Dusun</th>
    <td>:</td>
    <td><input type="text" class="form-control" name="Dusun" value="<?php echo $data_keluarga[0]['Dusun'] ?>" required></td>></td>
  </tr>
</table>

<button type="submit" class="btn btn-primary btn-lg">
  <i class="glyphicon glyphicon-floppy-save"></i> Simpan
</button>

</form>

<?php include('../_partials/bottom.php') ?>
