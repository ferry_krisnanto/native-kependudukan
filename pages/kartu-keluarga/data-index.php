<?php
include('../../config/koneksi.php');

// ambil dari database
$query = "SELECT * FROM kk LEFT JOIN Penduduk ON kk.No_KK = Penduduk.No_KK";

$hasil = mysqli_query($db, $query);

$data_kk = array();

while ($row = mysqli_fetch_assoc($hasil)) {
  $data_kk[] = $row;
}

// hitung kartu keluarga
$query_jumlah_kartu_keluarga = "SELECT COUNT(*) AS total FROM kk";
$hasil_jumlah_kartu_keluarga = mysqli_query($db, $query_jumlah_kartu_keluarga);
$jumlah_kartu_keluarga = mysqli_fetch_assoc($hasil_jumlah_kartu_keluarga);
