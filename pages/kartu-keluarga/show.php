 <?php include('../_partials/top.php') ?>

<h1 class="page-header">Data Kartu Keluarga</h1>
<?php include('_partials/menu.php') ?>

<?php include('data-show.php') ?>

<table class="table table-striped table-middle">
  <tr>
    <th width="20%">No_KK</th>
    <td width="1%">:</td>
    <td><?php echo $data_keluarga[0]['No_KK'] ?></td>
  </tr>
  <tr>
    <th>Id Kepala Keluarga</th>
    <td>:</td>
    <td><?php echo $data_keluarga[0]['Id_Kepala_Keluarga'] ?></td>
  </tr>
  <tr>
    <th>Agama</th>
    <td>:</td>
    <td><?php echo $data_keluarga[0]['Agama'] ?></td>
  </tr>
  </tr>
    <th>Pendidikan</th>
    <td>:</td>
    <td><?php echo $data_keluarga[0]['Pendidikan'] ?></td>
  </tr>
    <th>Pekerjaan</th>
    <td>:</td>
    <td><?php echo $data_keluarga[0]['Pekerjaan'] ?></td>
  </tr>
  <tr>
    <th>Status kawin</th>
    <td>:</td>
    <td><?php echo $data_keluarga[0]['Status_perkawinan'] ?></td>
  </tr>
  <tr>
    <th width="20%">Alamat</th>
    <td width="1%">:</td>
    <td><?php echo $data_keluarga[0]['Alamat'] ?></td>
  </tr>
  <tr>
    <th>RT</th>
    <td>:</td>
    <td><?php echo $data_keluarga[0]['RT'] ?></td>
  </tr>
  <tr>
    <th>RW</th>
    <td>:</td>
    <td><?php echo $data_keluarga[0]['RW'] ?></td>
  </tr>
    <th>Dusun</th>
    <td>:</td>
    <td><?php echo $data_keluarga[0]['Dusun'] ?></td>
  </tr>
  
  
<table class="table table-striped table-condensed table-hover" id="datatable">
  <thead>
    <tr>
      <th>No</th>
      <th>NIK</th>
      <th>Nama</th>
      <th>Tempat Lahir</th>
      <th>Tanggal Lahir</th>
      <th>Pendidikan</th>
      <th>Pekerjaan</th>
      <th>Status Kawin</th>
      <th>Aksi</th>
    </tr>
  </thead>
  <tbody>
    <?php $nomor = 1; ?>
    <?php foreach ($data_anggota_keluarga as $anggota_keluarga) : ?>
    <tr>
      <td><?php echo $nomor++ ?>.</td>
      <td><?php echo $anggota_keluarga['NIK'] ?></td>
      <td><?php echo $anggota_keluarga['Nama'] ?></td>
      <td><?php echo $anggota_keluarga['Tempat_lahir'] ?></td>
      <td>
        <?php echo ($anggota_keluarga['Tanggal_lahir'] != '0000-00-00') ? date('d-m-Y', strtotime($anggota_keluarga['Tanggal_lahir'])) : ''?>
      </td>
      <td><?php echo $anggota_keluarga['Pendidikan'] ?></td>
      <td><?php echo $anggota_keluarga['Pekerjaan'] ?></td>
      <td><?php echo $anggota_keluarga['Status_perkawinan'] ?></td>
      <td>
        <!-- Single button -->
        <div class="btn-group pull-right">
          <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
          <span class="caret"></span>
          </button>
          <ul class="dropdown-menu pull-right" role="menu">
            <li>
              <a href="../warga/show.php?id_warga=<?php echo $anggota_keluarga['uuid'] ?>">
                <span class="glyphicon glyphicon-sunglasses"></span> Detail
              </a>
            </li>
          </ul>
        </div>
      </td>
    </tr>
    <?php endforeach ?>
  </tbody>
</table>

<?php include('../_partials/bottom.php') ?>
