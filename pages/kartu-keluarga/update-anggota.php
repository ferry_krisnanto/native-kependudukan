 <?php
session_start();
if (!isset($_SESSION['user'])) {
  // jika user belum login
  header('Location: ../login');
  exit();
}

include('../../config/koneksi.php');

// ambil data dari form
$No_KTP = htmlspecialchars($_POST['No_KTP']);
$No_KK = htmlspecialchars($_POST['No_KK']);

// cek jika relasi sudah ada (sudah menjadi anggota)
$query_cek = "SELECT COUNT(*) AS total FROM Penduduk WHERE No_KTP = $No_KTP AND No_KK = $No_KK";
$hasil_cek = mysqli_query($db, $query_cek);
$jumlah_cek = mysqli_fetch_assoc($hasil_cek);

echo $jumlah_cek['total'];

if ($jumlah_cek['total'] != 0) {
  echo "<script>window.alert('Penduduk sudah menjadi anggota!'); window.location.href='../kk/edit-anggota.php?No_KK=$No_KK'</script>";
  exit;
}

// tambahkan ke pivot database
$query = "INSERT INTO Penduduk (No_KTP, No_KK) VALUES ('$No_KTP', '$No_KK');";

$hasil = mysqli_query($db, $query);

// cek keberhasilan pendambahan data
if ($hasil == true) {
  echo "<script>window.location.href='../kk/edit-anggota.php?No_KK=$No_KK'</script>";
} else {
  echo "<script>window.alert('Gagal menambahkan anggota!'); window.location.href='../kk/edit-anggota.php?No_KK=$No_KK'</script>";
}
