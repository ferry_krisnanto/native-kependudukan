<?php
session_start();
if (!isset($_SESSION['user'])) {
  // jika user belum login
  header('Location: ../login');
  exit();
}

include('../../config/koneksi.php');

// ambil data dari form
$No_KTP = htmlspecialchars($_GET['No_KTP']);
$No_KK = htmlspecialchars($_GET['No_KK']);

// delete database
$query = "DELETE FROM Penduduk WHERE No_KTP = $No_KTP";

$hasil = mysqli_query($db, $query);

// cek keberhasilan pendambahan data
if ($hasil == true) {
  echo "<script>window.location.href='../kk/edit-anggota.php?No_KK=$No_KK'</script>";
} else {
  echo "<script>window.alert('Data anggota gagal dihapus!'); window.location.href='../kk/edit-anggota.php?No_KK=$No_KK'</script>";
}
