<?php
session_start();
if (!isset($_SESSION['user'])) {
  // jika user belum login
  header('Location: ../login');
  exit();
}

include('../../config/koneksi.php');
include('../_partials/uuid.php');

// ambil data dari form
$No_KK = htmlspecialchars($_POST['No_KK']);
$Id_kepala_keluarga = htmlspecialchars($_POST['uuid']);
$Agama = htmlspecialchars($_POST['Agama']);
$Pendidikan = htmlspecialchars($_POST['Pendidikan']);
$Pekerjaan = htmlspecialchars($_POST['Pekerjaan']);
$Status_Kawin = htmlspecialchars($_POST['Status_perkawinan']);

$Alamat = htmlspecialchars($_POST['Alamat']);
$RT = htmlspecialchars($_POST['RT']);
$RW = htmlspecialchars($_POST['RW']);
$Dusun = htmlspecialchars($_POST['Dusun']);

$uuid = gen_uuid();

// masukkan ke database

$query = "INSERT INTO Penduduk (uuid, Agama, Pendidikan, Pekerjaan, Status_Kawin) VALUES ( '$uuid', '$Agama', '$Pendidikan', '$Pekerjaan', '$Status_perkawinan');";
$hasil = mysqli_query($db, $query);

$end = false;
if($hasil){
	$query2 = "INSERT INTO kk (No_KK, id_kepala_keluarga, Alamat, RT, RW, Dusun) VALUES (NULL, '$No_KK', '$uuid','$Alamat', '$RT', '$RW', '$Dusun');";
	$end 	= mysqli_query($db, $query2);
}

// id terakhir
// mysqli_insert_id($db)

// cek keberhasilan pendambahan data
if ($hasil) {
  echo "<script>window.alert('Tambah kartu keluarga berhasil'); window.location.href='../kk/create.php'</script>";
} else {
  echo "<script>window.alert('Tambah kartu keluarga gagal!'); window.location.href='../kk/create.php'</script>";
}
