<?php include('../_partials/top.php') ?>

<h1 class="page-header">Data Kartu Keluarga</h1>
<?php include('_partials/menu.php') ?>

<?php include('data-edit-anggota.php') ?>

<table class="table table-striped table-middle">
  <tr>
    <th width="20%">Nomor Kartu Keluarga</th>
    <td width="1%">:</td>
    <td><?php echo $data_keluarga[0]['No_KK'] ?></td>
  </tr>
  <tr>
    <th>Kepala Keluarga</th>
    <td>:</td>
    <td><?php echo $data_keluarga[0]['No_KK'] ?></td>
  </tr>
  <tr>
    <th>NIK Kepala Keluarga</th>
    <td>:</td>
    <td><?php echo $data_keluarga[0]['No_KTP'] ?></td>
  </tr>
</table>

<h3>Daftar Nama Warga</h3>
<form action="update-anggota.php" method="post">
  <table class="table table-striped table-middle">
    <tr>
      <th width="20%">Nama Warga</th>
      <td width="1%">:</td>
      <td>
        <select class="form-control selectlive" name="No_KTP" required>
          <option value="" selected disabled>- pilih -</option>
          <?php foreach ($data_Penduduk as $Penduduk) : ?>
          <option value="<?php echo $Penduduk['No_KTP'] ?>">
            <?php echo $Penduduk['Nama'] ?> (NIK: <?php echo $Penduduk['nik_Penduduk'] ?>)
          </option>
          <?php endforeach ?>
        </select>
      </td>
    </tr>
  </table>

  <input type="hidden" name="No_KK" value="<?php echo $get_No_KK ?>">

  <button type="submit" class="btn btn-primary btn-lg">
    <i class="glyphicon glyphicon-plus"></i> Tambahkan
  </button>
</form>

<br><br>

<h3>Data Anggota Kartu Keluarga</h3>
<table class="table table-striped table-condensed table-hover" id="datatable">
  <thead>
    <tr>
      <th>#</th>
      <th>NIK</th>
      <th>Nama</th>
      <th>Tempat Lahir</th>
      <th>Tanggal Lahir</th>
      <th>Pendidikan</th>
      <th>Pekerjaan</th>
      <th>Status Kawin</th>
      <th>Aksi</th>
    </tr>
  </thead>
  <tbody>
    <?php $nomor = 1; ?>
    <?php foreach ($data_anggota_keluarga as $anggota_keluarga) : ?>
    <tr>
      <td><?php echo $nomor++ ?>.</td>
      <td><?php echo $anggota_keluarga['No_KTP'] ?></td>
      <td><?php echo $anggota_keluarga['Nama'] ?></td>
      <td><?php echo $anggota_keluarga['Tempat_lahir'] ?></td>
      <td>
        <?php echo ($anggota_keluarga['Tanggal_lahir'] != '0000-00-00') ? date('d-m-Y', strtotime($anggota_keluarga['Tanggal_lahir'])) : ''?>
      </td>
      <td><?php echo $anggota_keluarga['Pendidikan'] ?></td>
      <td><?php echo $anggota_keluarga['Pekerjaan'] ?></td>
      <td><?php echo $anggota_keluarga['Status_perkawinan'] ?></td>
      <td>
        <!-- Single button -->
        <div class="btn-group pull-right">
          <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
          <span class="caret"></span>
          </button>
          <ul class="dropdown-menu pull-right" role="menu">
            <li>
              <a href="../Penduduk/show.php?No_KTP=<?php echo $anggota_keluarga['No_KTP'] ?>">
                <span class="glyphicon glyphicon-sunglasses"></span> Detail
              </a>
            </li>
            <li class="divider"></li>
            <li>
              <a href="../kk/delete-anggota.php?No_KTP=<?php echo $anggota_keluarga['No_KTP'] ?>&No_KK=<?php echo $data_keluarga[0]['No_KK'] ?>" onclick="return confirm('Yakin hapus dari anggota?')">
                <span class="glyphicon glyphicon-trash"></span> Hapus dari Anggota
              </a>
            </li>
          </ul>
        </div>
      </td>
    </tr>
    <?php endforeach ?>
  </tbody>
</table>

<?php include('../_partials/bottom.php') ?>
