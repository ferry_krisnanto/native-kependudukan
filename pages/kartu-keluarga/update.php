<?php
session_start();
if (!isset($_SESSION['user'])) {
  // jika user belum login
  header('Location: ../login');
  exit();
}

include('../../config/koneksi.php');

// ambil data dari form
$nomor_kartu_keluarga = htmlspecialchars($_POST['No_KK']);
$id_kepala_keluarga = htmlspecialchars($_POST['id_kepala_keluarga']);
$Agama = htmlspecialchars($_POST['Agama']);
$Pendidikan = htmlspecialchars($_POST['Pendidikan']);
$Pekerjaan = htmlspecialchars($_POST['Pekerjaan']);
$Status Kawin = htmlspecialchars($_POST['Status_perkawinan']);

$Alamat = htmlspecialchars($_POST['Alamat']);
$RT = htmlspecialchars($_POST['RT']);
$RW = htmlspecialchars($_POST['RW']);
$Dusun = htmlspecialchars($_POST['Dusun']);

$No_KK = htmlspecialchars($_POST['No_KK]);

$id_user = $_SESSION['user']['id_user'];

// masukkan ke database

$query = "UPDATE kk SET No_KK = '$No_KK', id_kepala_keluarga = '$id_kepala_keluarga', Agama = '$Agama', Pendidikan = '$Pendidikan', Pekerjaan = '$Pekerjaan', Status_perkawinan = '$Status_perkawinan', Alamat = '$Alamat', RT = '$RT', RW = '$RW', Dusun = '$Dusun', id_user = '$id_user', updated = CURRENT_TIMESTAMP WHERE kk.No_KK = $No_KK;";

$hasil = mysqli_query($db, $query);

// cek keberhasilan pendambahan data
if ($hasil == true) {
  echo "<script>window.alert('Ubah kartu keluarga berhasil'); window.location.href='../kk/'</script>";
} else {
  echo "<script>window.alert('Ubah kartu keluarga gagal!'); window.location.href='../kk/'</script>";
}
