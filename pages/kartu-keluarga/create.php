  <?php include('../_partials/top.php') ?>

<h1 class="page-header">Data Kartu Keluarga</h1>
<?php include('_partials/menu.php') ?>


<form action="store.php" method="post">
<table class="table table-striped table-middle">
  <tr>
    <th width="20%">No KK</th>
    <td width="1%">:</td>
    <td><input type="text" class="form-control" name="No_KK" required></td>
  </tr>
  <tr>
    <th>ID Kepala Keluarga</th>
    <td>:</td>
    <td>
      <select class="form-control selectlive" name="Id_Kepala_Keluarga" >
        <option value="" selected disabled>- pilih -</option>
        <?php foreach ($data_penduduk as $penduduk) : ?>
        <option value="<?php echo $penduduk['uuid'] ?>">
          <?php echo $penduduk['Nama'] ?> (NIK: <?php echo $penduduk['NIK'] ?>)
        </option>
        <?php endforeach ?>
      </select>
    </td>
  </tr>
   <tr>
    <th width="20%">Jumlah Anggota Keluarga</th>
    <td width="1%">:</td>
    <td><input type="text" class="form-control" name="No_KK" ></td>
  </tr>
</table>
 <table class="table table-striped table-middle">
  <tr>
   <tr>
    <th width="20%">ID Anggota Keluarga (istri)</th>
    <td width="1%">:</td>
    <td>
      <select class="form-control selectlive" name="Id_Kepala_Keluarga" >
        <option value="" selected disabled>- pilih -</option>
        <?php foreach ($data_penduduk as $penduduk) : ?>
        <option value="<?php echo $penduduk['uuid'] ?>">
          <?php echo $penduduk['Nama'] ?> (NIK: <?php echo $penduduk['NIK'] ?>)
        </option>
        <?php endforeach ?>
      </select>
    </td>
  </tr>
  <tr>
    <th>ID Anggota Keluarga (Anak)</th>
    <td>:</td>
    <td>
      <select class="form-control selectlive" name="Id_Kepala_Keluarga" >
        <option value="" selected disabled>- pilih -</option>
        <?php foreach ($data_penduduk as $penduduk) : ?>
        <option value="<?php echo $penduduk['uuid'] ?>">
          <?php echo $penduduk['Nama'] ?> (NIK: <?php echo $penduduk['NIK'] ?>)
        </option>
        <?php endforeach ?>
      </select>
    </td>
  </tr>
  <tr>
    <th width="20%">Agama</th>
    <td width="1%">:</td>
    <td>
      <select class="form-control selectlive" name="Agama" required>
        <option value="" selected disabled>- pilih -</option>
        <option value="Islam">Islam</option>
        <option value="Kristen">Kristen</option>
        <option value="Katholik">Katholik</option>
        <option value="Hindu">Hindu</option>
        <option value="Budha">Budha</option>
        <option value="Konghucu">Konghucu</option>
      </select>
    </td>
  </tr>
  <tr>
    <th>Pendidikan</th>
    <td>:</td>
    <td>
      <select class="form-control selectlive" name="Pendidikan" required>
        <option value="" selected disabled>- pilih -</option>
        <option value="Tidak Sekolah">Tidak Sekolah</option>
        <option value="Tidak Tamat SD">Tidak Tamat SD</option>
        <option value="SD">SD</option>
        <option value="SMP">SMP</option>
        <option value="SMA">SMA</option>
        <option value="D1">D1</option>
        <option value="D2">D2</option>
        <option value="D3">D3</option>
        <option value="S1">S1</option>
        <option value="S2">S2</option>
        <option value="S3">S3</option>
      </select>
    </td>
  </tr>
  <tr>
    <th>Pekerjaan</th>
    <td>:</td>
    <td><input type="text" class="form-control" name="Pekerjaan" required></td>
  </tr>
  <tr>
    <th>Status kawin</th>
    <td>:</td>
    <td>
      <select class="form-control selectpicker" name="Status_perkawinan" required>
        <option value="" selected disabled>- pilih -</option>
        <option value="Kawin">Kawin</option>
        <option value="Belum Kawin">Belum Kawin</option>
      </select>
    </td>
  </tr>
  <tr>
    <th width="20%">Alamat</th>
    <td width="1%">:</td>
    <td><textarea class="form-control" name="Alamat"></textarea></td>
  </tr>
  <tr>
    <th>RT</th>
    <td>:</td>
    <td><input type="text" class="form-control" name="RT" required></td>
  </tr>
  <tr>
    <th>RW</th>
    <td>:</td>
    <td><input type="text" class="form-control" name="RW" required></td>
  </tr>
  <tr>
    <th>Dusun</th>
    <td>:</td>
    <td><input type="text" class="form-control" name="Dusun"></td>
  </tr>
</table>

<button type="submit" class="btn btn-primary btn-lg">
  <i class="glyphicon glyphicon-floppy-save"></i> Simpan
</button>
</form>

<?php include('../_partials/bottom.php') ?>
