<?php include('../_partials/top.php') ?>

<h1 class="page-header">Data Kartu Keluarga</h1>
<?php include('_partials/menu.php') ?>

<?php include('data-index.php') ?>

<table class="table table-striped table-condensed table-hover" id="datatable">
  <thead>
    <tr>
      <th>No</th>
      <th>No_KK</th>
      <th>Id Kepala Keluarga</th>
      <th>Jml. Anggota kel.</th>
      <th>Agama</th>
      <th>Pendidikan</th>
      <th>Pekerjaan</th>
      <th>Status Kawin</th>
      <th>Alamat</th>
      <th>RT</th>
      <th>RW</th>
      <th>Dusun</th>
      <th>Aksi</th>
    </tr>
  </thead>
  <tbody>
    <?php $nomor = 1; ?>
    <?php foreach ($data_kk as $kk) : ?>

    <?php
    // hitung anggota
    $query_jumlah_anggota = "SELECT COUNT(*) AS total FROM Penduduk WHERE No_KK = ".$kk['No_KK'];
    $hasil_jumlah_anggota = mysqli_query($db, $query_jumlah_anggota);
    $jumlah_jumlah_anggota = mysqli_fetch_assoc($hasil_jumlah_anggota);
    ?>

    <tr>
      <td><?php echo $nomor++ ?>.</td>
      <td><?php echo $kk['No_KK'] ?></td>
      <td><?php echo $kk['Id_Kepala_Keluarga'] ?></td>
      <td><?php echo $jumlah_jumlah_anggota['total'] ?></td>
      <td><?php echo $kk['Agama'] ?></td>
      <td><?php echo $kk['Pendidikan'] ?></td>
      <td><?php echo $kk['Pekerjaan'] ?></td>
      <td><?php echo $kk['Status_perkawinan'] ?></td>
      <td><?php echo $kk['Alamat'] ?></td>
      <td><?php echo $kk['RT'] ?></td>
      <td><?php echo $kk['RW'] ?></td>
      <td><?php echo $kk['Dusun'] ?></td>
      <td>
        <!-- Single button -->
        <div class="btn-group pull-right">
          <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
          <span class="caret"></span>
          </button>
          <ul class="dropdown-menu pull-right" role="menu">
            <li>
              <a href="show.php?No_KK=<?php echo $kk['No_KK'] ?>"><span class="glyphicon glyphicon-sunglasses"></span> Detail</a>
            </li>
            <li>
              <a href="cetak-show.php?No_KK=<?php echo $kk['No_KK'] ?>" target="_blank"><span class="glyphicon glyphicon-print"></span> Cetak</a>
            </li>
            <?php if ($_SESSION['user']['status'] != 'Admin'): ?>
            <li class="divider"></li>
            <li>
              <a href="edit-anggota.php?No_KK=<?php echo $kk['No_KK'] ?>"><span class="glyphicon glyphicon-list"></span> Ubah Anggota</a>
            </li>
            <li class="divider"></li>
            <li>
              <a href="edit.php?No_KK=<?php echo $kk['No_KK'] ?>"><span class="glyphicon glyphicon-edit"></span> edit</a>
            </li>
            <li class="divider"></li>
            <li>
              <a href="delete.php?No_KK=<?php echo $kk['No_KK'] ?>" onclick="return confirm('Yakin hapus data ini?')">
                <i class="glyphicon glyphicon-trash"></i> Hapus
              </a>
            </li>
            <?php endif; ?>
          </ul>
        </div>
      </td>
    </tr>
    <?php endforeach ?>
  </tbody>
</table>

<br><br>

<div class="well">
  <dl class="dl-horizontal">
    <dt>Total Kartu Keluarga</dt>
    <dd><?php echo $jumlah_kartu_keluarga['total'] ?> keluarga</dd>
  </dl>
</div>

<?php include('../_partials/bottom.php') ?>
