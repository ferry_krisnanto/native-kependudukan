<?php include('../_partials/top.php') ?>

<h1 class="page-header">Data Kematian</h1>
<?php include('_partials/menu.php') ?>

<?php include('data-index.php') ?>
<?php include('../Beranda/data-index.php') ?>

<table class="table table-striped table-condensed table-hover" id="datatable">
  <thead>
    <tr>
      <th>No</th>
      <th>NIK</th>
      <th>Nama</th>
      <th>Tempat Lahir</th>
      <th>Tanggal lahir</th>
      <th>L/P</th>
      <th>Alamat</th>
      <th>Tempat Meninggal</th>
      <th>Tanggal Meninggal</th>
      <th>Waktu Meninggal</th>
      <th>Aksi</th>
    </tr>
  </thead>
  <tbody>
    <?php $nomor = 1; ?>
    <?php foreach ($data_Kematian as $Kematian) : ?>
    <tr>
      <td><?php echo $nomor++ ?>.</td>
      <td><?php echo $Kematian['NIK'] ?></td>
      <td><?php echo $Kematian['Nama'] ?></td>
      <td><?php echo $Kematian['Tempat_lahir'] ?></td>
      <td><?php echo $Kematian['Tanggal_lahir'] ?></td>
      <td><?php echo $Kematian['Jenis_kelamin'] ?></td>
      <td><?php echo $Kematian['Alamat'] ?></td>
      <td><?php echo $Kematian['Tempat_Meninggal'] ?></td>
      <td><?php echo $Kematian['Tanggal_Meninggal'] ?></td>
      <td><?php echo $Kematian['Waktu_Meninggal'] ?></td>
      <td>
        <!-- Single button -->
        <div class="btn-group pull-right">
          <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
          <span class="caret"></span>
          </button>
          <ul class="dropdown-menu pull-right" role="menu">
            <li>
              <a href="show.php?NIK=<?php echo $Kematian['NIK'] ?>"><i class="glyphicon glyphicon-sunglasses"></i> Detail</a>
            </li>
            <li>
              <a href="cetak-show.php?NIK=<?php echo $Kematian['NIK'] ?>" target="_blank"><i class="glyphicon glyphicon-print"></i> Cetak</a>
            </li>
            <?php if ($_SESSION['user']['status'] == 'admin'): ?>
            <li class="divider"></li>
            <li>
              <a href="edit.php?NIK=<?php echo $Kematian['NIK'] ?>"><i class="glyphicon glyphicon-edit"></i> edit</a>
            </li>
            <li class="divider"></li>
            <li>
              <a href="delete.php?NIK=<?php echo $Kematian['NIK'] ?>" onclick="return confirm('Yakin hapus data ini?')">
                <i class="glyphicon glyphicon-trash"></i> Hapus
              </a>
            </li>
            <?php endif; ?>
          </ul>
        </div>
      </td>
    </tr>
    <?php endforeach ?>
  </tbody>
</table>


<?php include('../_partials/bottom.php') ?>
