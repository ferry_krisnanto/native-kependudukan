<?php include('../_partials/top.php') ?>

<h1 class="page-header">Data Kematian</h1>
<?php include('_partials/menu.php') ?>

<?php include('data-show.php') ?>

<table class="table table-striped">
  <tr>
    <th width="20%">NIK</th>
    <td width="1%">:</td>
    <td><?php echo $data_Kematian[0]['NIK'] ?></td>
  </tr>
  <tr>
    <th>Nama</th>
    <td>:</td>
    <td><?php echo $data_Kematian[0]['Nama'] ?></td>
  </tr>
  <tr>
    <th>Tempat Lahir</th>
    <td>:</td>
    <td><?php echo $data_Kematian[0]['Tempat_lahir'] ?></td>
  </tr>
  <tr>
    <th>Tanggal Lahir</th>
    <td>:</td>
    <td><?php echo $data_Kematian[0]['Tanggal_lahir'] ?></td>
    </td>
  </tr>
  <tr>
    <th>Jenis Kelamin</th>
    <td>:</td>
    <td><?php echo $data_Kematian[0]['Jenis_kelamin'] ?></td>
  </tr>
  <tr>
    <th width="20%">Alamat</th>
    <td width="1%">:</td>
    <td><?php echo $data_Kematian[0]['Alamat'] ?></td>
  </tr>
  <tr>
    <th>Tempat Meninggal</th>
    <td>:</td>
    <td><?php echo $data_Kematian[0]['Tempat_Meninggal'] ?></td>
  </tr>
  <tr>
    <th>Tanggal Meninggal</th>
    <td>:</td>
    <td><?php echo $data_Kematian[0]['Tanggal_Meninggal'] ?></td>
    </td>
  </tr>
  <tr>
    <th>Waktu Meninggal</th>
    <td>:</td>
    <td><?php echo $data_Kematian[0]['Tempat_Meninggal'] ?></td>
  </tr>
</table>

<?php include('../_partials/bottom.php') ?>
