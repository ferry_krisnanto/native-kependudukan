<?php include('../_partials/top.php') ?>

<h1 class="page-header">Data Kematian</h1>
<?php include('_partials/menu.php') ?>


<form action="store.php" method="post">
<table class="table table-striped table-middle">
  <tr>
    <th width="20%">NIK</th>
    <td width="1%">:</td>  
    <td><input type="text" class="form-control" name="NIK" required></td>
  </tr>
  <tr>
    <th>Nama</th>
    <td>:</td>
    <td><input type="text" class="form-control" name="Nama" ></td>
  </tr>
  <tr>
    <th>Tempat Lahir</th>
    <td>:</td>
    <td><input type="text" class="form-control" name="Tempat_lahir"></td>
  </tr>
  <tr>
    <th>Tanggal Lahir</th>
    <td>:</td>
    <td><input type="text" class="form-control datepicker" name="Tanggal_lahir"></td>
  </tr>
   <tr>
    <th>Jenis Kelamin</th>
    <td>:</td>
    <td>
      <select class="form-control selectpicker" name="Jenis_kelamin">
        <option value="" selected disabled>- pilih -</option>
        <option value="L">Laki-laki</option>
        <option value="P">Perempuan</option>
      </select>
    </td>
  </tr>
  <tr>
    <th width="20%">Alamat</th>
    <td width="1%">:</td>
    <td><textarea class="form-control" name="Alamat"></textarea></td>
  </tr>
  <tr>
    <th>Tempat Meninggal</th>
    <td>:</td>
    <td><input type="text" class="form-control" name="Tempat_Meninggal" required></td>
  </tr>
  <tr>
    <th>Tanggal Meninggal</th>
    <td>:</td>
    <td><input type="text" class="form-control datepicker" name="Tanggal_Meninggal" required></td>
  </tr>
  <tr>
    <th>Waktu Meninggal</th>
    <td>:</td>
    <td><input type="text" class="form-control" name="Waktu_Meninggal" required></td>
  </tr>
</table>

<button type="submit" class="btn btn-primary btn-lg">
  <i class="glyphicon glyphicon-export"></i> Kematian  
</button>
</form>

<?php include('../_partials/bottom.php') ?>
