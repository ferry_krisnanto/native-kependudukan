<?php include('../_partials/top.php') ?>

<h1 class="page-header">Data Kematian</h1>
<?php include('_partials/menu.php') ?>

<?php include('data-show.php') ?>

<form action="update.php" method="post">
<table class="table table-striped table-middle">
  <tr>
    <th width="20%">NIK</th>
    <td width="1%">:</td>  
    <td><input type="text" class="form-control" name="NIK" value="<?php echo $data_Kematian[0]['NIK']; ?>"required></td>
  </tr>
  <tr>
    <th>Nama</th>
    <td>:</td>
    <td><input type="text" class="form-control" name="Nama" value="<?php echo $data_Kematian[0]['Nama'] ?>" required></td>
  </tr>
  <tr>
    <th>Tempat Lahir</th>
    <td>:</td>
    <td><input type="text" class="form-control" name="Tempat_lahir" value="<?php echo $data_Kematian[0]['Tempat_lahir']; ?>"required></td>
  </tr>
  <tr>
    <th>Tanggal Lahir</th>
    <td>:</td>
    <td><input type="text" class="form-control datepicker" name="Tanggal_lahir" value="<?php echo $data_Kematian[0]['Tanggal_lahir'] ?>"required></td>
  </tr>
   <tr>
    <th>Jenis Kelamin</th>
    <td>:</td>
    <td>
      <select class="form-control selectpicker" name="Jenis_kelamin" required>
	  value="<?php echo $data_Kematian[0]['Jenis_kelamin'] ?>"
	  <?php
		if($data_Kematian[0]['Jenis_kelamin'] == 'L'){
	  ?>
		<option value="L">Laki-laki</option>
        <option value="P">Perempuan</option>
	  <?php
		}else{
	  ?>
        <option value="P">Perempuan</option>
		<option value="L">Laki-laki</option>
	  <?php
		}
	  ?>
      </select>
    </td>
  </tr>
  <tr>
    <th width="20%">Alamat</th>
    <td width="1%">:</td>
    <td><textarea class="form-control" name="Alamat"><?php echo $data_Kematian[0]['Alamat'] ?></textarea></td>
  </tr>
  <tr>
    <th>Tempat Meninggal</th>
    <td>:</td>
    <td><input type="text" class="form-control" name="Tempat_Meninggal" value="<?php echo $data_Kematian[0]['Tempat_Meninggal'] ?>"required></td>
  </tr>
  <tr>
    <th>Tanggal Meninggal</th>
    <td>:</td>
    <td><input type="text" class="form-control datepicker" name="Tanggal_Meninggal" value="<?php echo $data_Kematian[0]['Tanggal_Meninggal'] ?>"required></td>
  </tr>
  <tr>
    <th>Waktu Meninggal</th>
    <td>:</td>
    <td><input type="text" class="form-control" name="Waktu_Meninggal" value="<?php echo $data_Kematian[0]['Waktu_Meninggal'] ?>"required></td>
  </tr>
</table>

<button type="submit" class="btn btn-primary btn-lg">
  <i class="glyphicon glyphicon-floppy-save"></i> Simpan
</button>
</form>

<?php include('../_partials/bottom.php') ?>
