<?php
session_start();
if (!isset($_SESSION['user'])) {
  // jika user belum login
  header('Location: ../login');
  exit();
}

include('../../config/koneksi.php');
include('../_partials/uuid.php');

// ambil data dari form
$NIK = htmlspecialchars($_POST['NIK']);
$Nama = htmlspecialchars($_POST['Nama']);
$Tempat_lahir = htmlspecialchars($_POST['Tempat_lahir']);
$Tanggal_lahir = htmlspecialchars($_POST['Tanggal_lahir']);
$Jenis_kelamin = htmlspecialchars($_POST['Jenis_kelamin']);

$Alamat = htmlspecialchars($_POST['Alamat']);
$Tempat_Meninggal = htmlspecialchars($_POST['Tempat_Meninggal']);
$Tanggal_Meninggal = htmlspecialchars($_POST['Tanggal_Meninggal']);
$Waktu_Meninggal = htmlspecialchars($_POST['Waktu_Meninggal']);

$uuid = gen_uuid();

// masukkan ke database

$query = "INSERT INTO penduduk ( uuid, NIK, Nama, Tempat_lahir, Tanggal_lahir, Jenis_kelamin, Alamat) VALUES ( '$uuid', '$NIK' '$Nama', '$Tempat_lahir', '$Tanggal_lahir', '$Jenis_kelamin', '$Alamat');";
$hasil = mysqli_query($db, $query);

if($hasil){
	$query2 = "INSERT INTO kematian ( NIK, Tempat_Meninggal, Tanggal_Meninggal, Waktu_Meninggal) VALUES ( '$Tempat_Meninggal', '$Tanggal_Meninggal', '$Waktu_Meninggal');";
	$end 	= mysqli_query($db, $query2);
}

// cek keberhasilan penambahan data
if ($hasil == true) {
  echo "<script>window.alert('Data kematian berhasil ditambahkan'); window.location.href='../Kematian/'</script>";
} else {
  echo "<script>window.alert('Data kematian Gagal ditambahkan!'); window.location.href='../Kematian/create.php?NIK=".$NIK."'</script>";
}
